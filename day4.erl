-module(day4).

-export([read_input/1, sleepy_guard/1, schedules/1, sleepiest_minute/1]).

% note: not robust
shift_starts(String) ->
    {ok, [Id], _} = io_lib:fread(" #~d begins shift", String),
    {shift_starts, Id}.

read_lines(InputFile) ->
    {ok, Input} = file:open(InputFile, read),
    Lines = read_lines(Input, []),
    ok = file:close(Input),
    Lines.
read_lines(Input, Lines) ->
    case file:read_line(Input) of
        eof -> lists:reverse(Lines);
        {ok, Line} -> read_lines(Input, [Line|Lines])
    end.

parse_input(Lines) ->
    lists:map(fun(Line) -> 
                      case io_lib:fread("[1518-~d-~d ~d:~d] ~s", Line) of
                          {ok, [Month, Day, Hour, Minute, "Guard"], Rest} ->
                              {{Month, Day, Hour, Minute}, shift_starts(Rest)};
                          {ok, [Month, Day, Hour, Minute, "wakes"], _Rest} ->
                              {{Month, Day, Hour, Minute}, awake};
                          {ok, [Month, Day, Hour, Minute, "falls"], _Rest} ->
                              {{Month, Day, Hour, Minute}, asleep}
                      end
              end,
              Lines).

read_input(File) ->
    Lines = read_lines(File),
    lists:keysort(1, parse_input(Lines)).

schedules(Input) ->
    PlusOne = fun(X) -> X+1 end,
    {_, Schedule} = 
        lists:foldl(fun({{_,_,_,_}, {shift_starts, Guard}}, {_, Schedule}) ->
                            {Guard, Schedule};
                       ({{_,_,_,M}, asleep}, {Guard, Schedule}) ->
                            {Guard, {asleep, M}, Schedule};
                       ({{_,_,_,M}, awake}, {Guard, {asleep, Start}, Schedule}) ->
                            MarkMinutes = 
                                fun(S) -> 
                                        lists:foldl(
                                          fun(Minute, Acc) -> 
                                                  maps:update_with(Minute, PlusOne, 1, Acc)
                                          end, S, lists:seq(Start, M-1))
                                end,
                        {Guard, maps:update_with(Guard, MarkMinutes, #{}, Schedule)}
                    end, {-1, #{}}, Input),
    Schedule.

% #{ <guard_id> => #{ <minute> => count } }
sleepy_guard(Schedules) ->
    maps:fold(fun(Guard, Sleep, Acc={_,_,SleepTime}) -> 
                      S = lists:sum(maps:values(Sleep)),
                      if S > SleepTime ->
                              {Guard,Sleep,S};
                         S =< SleepTime ->
                              Acc
                      end
              end, {-1, #{}, 0}, Schedules).

sleepiest_minute(Schedules) ->
    maps:fold(
     fun(Guard, Schedule, Acc={_, _, MaxSleep}) ->
             {Minute, GuardMaxSleep} = 
                 maps:fold(fun(Min, Freq, {_,F}) 
                                 when Freq > F -> {Min, Freq};
                              (_, _, MostFrequent) -> MostFrequent
                           end, {-1, 0}, Schedule),
             if GuardMaxSleep > MaxSleep -> {Guard, Minute, GuardMaxSleep};
                true -> Acc
             end
     end, {-1, 0, 0}, Schedules).
