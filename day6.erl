-module(day6).

-export([read_input/1, build_map/1, largest_area/1, area_less_than/2]).

-include_lib("eunit/include/eunit.hrl").

read_input(File) ->
    util:read_input(File, "~d,~d", 
                    fun([X,Y]) -> {X, Y} end).

bounds(Input) ->
    {Xs, Ys} = lists:unzip(Input),
    {{lists:min(Xs), lists:min(Ys)}, {lists:max(Xs), lists:max(Ys)}}.

manhattan_distance({X1,Y1}, {X2, Y2}) ->
    abs(X1 - X2) + abs(Y1 - Y2).

build_map(Input) ->
    Closest = 
        fun(P) ->
                case lists:keysort(2, [ {X, manhattan_distance(X, P)} || X <- Input ]) of
                    [{_, D}, {_, D}|_] ->
                        tied;
                    [{Coord, _}|_]     ->
                        Coord
                end
        end,
    {{Xmin, Ymin}, {Xmax, Ymax}} = bounds(Input),
    Grid = [ {Closest({X,Y}), {X,Y}} || X <- lists:seq(Xmin, Xmax),
                                        Y <- lists:seq(Ymin, Ymax) ],

    %% Convert the map list in to a map(point(), [point()])
    %% representing the set of grid locations that are closest to the
    %% given point. This can be filtered to remove all keys that are
    %% closest to a grid cell on the boundary of the map leaving a
    %% "bounded" map.

    Map = lists:foldl(fun({K, C}, BoundedMap) ->
                              maps:update_with(K, 
                                               fun(G) -> [C|G] end,
                                               [C],
                                               BoundedMap)
                      end, #{}, Grid),
    OnEdge = fun({X, Y}) ->
                     (X =:= Xmin) or (X =:= Xmax) or (Y =:= Ymin) or (Y =:= Ymax)
             end,
    maps:filter(
      fun(tied, _) -> false;
         (_, V) -> 
              lists:foldl(
                fun(C, Acc) -> 
                        Acc and (not OnEdge(C))
                end, true, V)
      end, Map).

largest_area(Map) ->
    lists:last(lists:keysort(2, maps:to_list(maps:map(fun(_, V) -> length(V) end, Map)))).

area_less_than(D, Input) ->
    DistanceToAll = fun(P) ->
                            lists:sum(
                              lists:map(
                                fun(C) -> manhattan_distance(P,C) end,
                                Input))
                    end,
    {{Xmin, Ymin}, {Xmax, Ymax}} = bounds(Input),
    length([ {X,Y} || X <- lists:seq(Xmin, Xmax),
                      Y <- lists:seq(Ymin, Ymax),
                      DistanceToAll({X,Y}) < D]).

bounds_test_() ->
    [?_assertEqual({{0,0},{0,0}}, bounds([{0,0}])),
     ?_assertEqual({{1,10}, {10, 11}}, bounds([{10, 10}, {1,11}]))].
