-module(day3).
-include_lib("eunit/include/eunit.hrl").
-export([read_input/1, total_overlap/1, isolated_claim/1]).
-record(claim, {id,
                x,
                y,
                width,
                height}).

total_overlap(Claims) ->
    Grid = lists:foldl(fun(#claim{x=X,y=Y,height=Height,width=Width}, Grid) ->
                               Cells = [ {X1, Y1} || X1 <- lists:seq(X, X+Width-1), Y1 <- lists:seq(Y, Y+Height-1)],
                               lists:foldl(fun(Cell, G) -> 
                                                   maps:update_with(Cell, 
                                                                    fun(0) -> 1;
                                                                       (K) -> K
                                                                    end,
                                                                    0, G)
                                           end, Grid, Cells)
                       end,
                       #{}, Claims),
    lists:sum(maps:values(Grid)).

isolated_claim(Claims) ->
    {_Grid, Overlaps} =
        lists:foldl(
          fun(#claim{id=Id,x=X,y=Y,height=H,width=W}, {Grid,Overlaps}) ->
                  Cells = [ {X1, Y1} || X1 <- lists:seq(X, X+W-1), 
                                        Y1 <- lists:seq(Y, Y+H-1)],
                  lists:foldl(fun(Cell,{G,O}) ->
                                      case maps:get(Cell, G, nil) of
                                          nil ->
                                              {maps:put(Cell, [Id], G),
                                               O};
                                          Ids -> 
                                              {maps:update(Cell, [Id|Ids], G),
                                               lists:foldl(fun(ClaimId, Overlap) ->
                                                                  gb_sets:add(ClaimId, Overlap)
                                                           end, O, [Id|Ids])}
                                      end
                              end, {Grid, Overlaps}, Cells)
          end, {#{}, gb_sets:empty()}, Claims),
    gb_sets:to_list(gb_sets:difference(gb_sets:from_list(lists:seq(1,length(Claims))), Overlaps)).
        

read_input(File) ->
    util:read_input(File, 
                    "#~d @ ~d,~d: ~dx~d", 
                    fun([N,X,Y,W,H]) ->
                            #claim{id = N,
                                   x  = X,
                                   y  = Y,
                                   width = W,
                                   height = H}
                    end).

overlap_test() ->
    Claims = [#claim{id = 1,x = 1,y = 3,width = 4,height = 4},
              #claim{id = 2,x = 3,y = 1,width = 4,height = 4},
              #claim{id = 3,x = 5,y = 5,width = 2,height = 2}],
    4 = total_overlap(Claims).

isolated_test() ->
    Claims = [#claim{id = 1,x = 1,y = 3,width = 4,height = 4},
              #claim{id = 2,x = 3,y = 1,width = 4,height = 4},
              #claim{id = 3,x = 5,y = 5,width = 2,height = 2}],
    3 = isolated_claim(Claims).
