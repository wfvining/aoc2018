-module(day8).

-export([read_input/1, make_tree/1, sum_metadata/1, value/1]).

-include_lib("eunit/include/eunit.hrl").

read_input(File) ->
    util:read_input(File, "~d", fun hd/1).

make_tree([N,M|Rest]) ->
    make_node(N, M, Rest).

children(0, Children, Rest) ->
    {lists:reverse(Children), Rest};
children(K, Children, [N,M|Rest]) ->
    {Node, Remaining} = make_node(N, M, Rest),
    children(K-1, [Node|Children], Remaining).

make_node(N, M, Rest) ->
    {Children, Remaining} = children(N, [], Rest),
    {MetaData, More} = lists:split(M, Remaining),
    {{Children, MetaData}, More}.

sum_metadata({Children, MetaData}) ->
    lists:sum(lists:map(fun sum_metadata/1, Children)) + lists:sum(MetaData).

value({[], MetaData}) ->
    lists:sum(MetaData);
value({Children, MetaData}) ->
    ChildValues = lists:map(fun value/1, Children),
    lists:foldl(fun(N, Acc) when N =< length(ChildValues) andalso N >= 1 -> Acc + lists:nth(N, ChildValues);
                   (_, Acc) -> Acc
                end, 0, MetaData).

value_test_() ->
    {ExampleTree, []} = make_tree([1, 1,  2, 4,   0, 1, 1,   0, 2, 1, 10,   1, 1, 1, 1,    1]),
    {AOCTree, []} = make_tree([2,3,0,3,10,11,12,1,1,0,1,99,2,1,1,2]),
    [ {"md only",
       ?_assertEqual(2, value({[], [1,1]}))},
      {"",
       ?_assertEqual(4, value(ExampleTree))},
      {"AOC Example", 
       ?_assertEqual(66, value(AOCTree))} ].
    
