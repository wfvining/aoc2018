-module(day5).

-export([read_input/1, react/1, find_blocker/1]).

-include_lib("eunit/include/eunit.hrl").

read_input(File) ->
    hd(hd(util:read_input(File, "~s", fun(X) -> X end))).

react([P|Polymer]) ->
    react([], P, Polymer).

react([P|Preceding], X, [F]) when abs(X - F) =:= 32 ->
    react(Preceding, P, []);
react([P], X, [F|Following]) when abs(X - P) =:= 32 ->
    react([], F, Following);

react(Preceding, X, [F,F2|Following]) when abs(X - F) =:= 32 -> 
    react(Preceding, F2, Following);
react([P,P2|Preceding], X, Following) when abs(X - P) =:= 32 ->
    react(Preceding, P2, Following);
react([P], X, [F|Following]) when abs(X - P) =:= 32 ->
    react([], F, Following);
react([P|Preceding], X, [F]) when abs(X - F) =:= 32 ->
    react(Preceding, P, []);
react([], X, [F]) when abs(X - F) =:= 32 ->
    [];
react([P], X, []) when abs(X - P) =:= 32 ->
    [];
react(Preceding, X, [F|Following]) ->
    react([X|Preceding], F, Following);
react(Preceding, X, []) ->
    lists:reverse([X|Preceding]).

find_blocker(Polymer) ->
    IsntType = fun(X) -> fun(C) -> X =/= C andalso abs(X - C) =/= 32 end end,
    hd(lists:sort(
         [ length(
             react(
               lists:filter(IsntType(C), Polymer))) 
           || C <- lists:seq($a, $z) ])).

react_test_() ->
    [?_assertEqual("a", react("a")),
     ?_assertEqual("aabbcc", react("aabbcc")),
     ?_assertEqual("aB", react("aAaBbB")),
     ?_assertEqual("", react("aABb")),
     ?_assertEqual("", react("dcbaABCD")),
     ?_assertEqual("A", react("AdcbaABCD")),
     ?_assertEqual("AA", react("AdcbaABCDA"))
    ].
    

    