-module(util).

-export([read_input/3]).

read_input(File, InputPattern, ParseFun) ->
    {ok, InputFile} = file:open(File, read),
    Input = process_input(InputFile, InputPattern, ParseFun, []),
    ok = file:close(InputFile),
    Input.

process_input(Input, InputPattern, ParseFun, Acc) ->
    case io:fread(Input, "", InputPattern) of
        {ok, In} ->
            process_input(Input, InputPattern, ParseFun,
                          [ParseFun(In) | Acc]);
        eof ->
            lists:reverse(Acc)
    end.

