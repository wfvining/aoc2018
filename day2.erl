-module(day2).

-export([read_input/1, checksum/1, single_error/1]).

read_input(File) ->
    util:read_input(File, "~s", fun hd/1).

double(Str) ->
    n_occurences(Str, 2, #{}).
triple(Str) ->
    n_occurences(Str, 3, #{}).

n_occurences([], N, Counts) ->
    S = maps:size(maps:filter(fun(_K, V) -> V =:= N end, Counts)),
    if S > 0 -> 1; S =:= 0 -> 0 end;
n_occurences([C|Str], N, Counts) ->
    n_occurences(Str, N, 
                 maps:update_with(
                   C,
                   fun(V) -> V + 1 end,
                   1,
                   Counts)).

checksum(Ids) ->
    {Doubles, Triples} = 
        lists:foldl(fun({Double,Triple}, {DoubleSum, TripleSum}) -> 
                            {DoubleSum + Double, TripleSum + Triple}
                    end, {0,0}, 
                    lists:map(fun(Str) -> 
                                      {double(Str), triple(Str)}
                              end, Ids)),
    Doubles * Triples.

differ_one(IdA, IdB) ->
    L = lists:zipwith(fun(A, B) when A =:= B -> 1;
                         (A, B) when A  /= B -> 0 
                      end, IdA, IdB),
    lists:sum(L) =:= length(L) - 1.

same([], []) ->
    [];
same([H|T1], [H|T2]) ->
    [H|same(T1,T2)];
same([_|T1], [_|T2]) ->
    same(T1,T2).

single_error(Ids) ->
    [H,_] = [ same(A, B) || A <- Ids, B <- Ids, differ_one(A,B)],
    H.
