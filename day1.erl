-module(day1).
-export([first_repeat/1]).
-include_lib("eunit/include/eunit.hrl").

first_repeat(Shifts) ->
    first_repeat(Shifts, Shifts, 0, gb_sets:empty()).

first_repeat([], Shifts, CurrentFreq, VisitedFreqs) ->
    first_repeat(Shifts, Shifts, CurrentFreq, VisitedFreqs); %% Didn't find it. Start over.
first_repeat([H|Tail], Shifts, CurrentFreq, VisitedFreqs) ->
    case gb_sets:is_element(CurrentFreq, VisitedFreqs) of
        true  ->
            CurrentFreq;
        false ->
            first_repeat(Tail, Shifts, CurrentFreq+H, gb_sets:add(CurrentFreq, VisitedFreqs))
    end.

fr_test() ->
    0 = first_repeat([-1,1]),
    10 = first_repeat([+3, +3, +4, -2, -4]),
    5  = first_repeat([-6, +3, +8, +5, -6]),
    14 = first_repeat([+7, +7, -2, -7, -4]).
