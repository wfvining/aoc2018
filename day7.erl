-module(day7).

-export([read_input/1, minimum_time/2]).
-compile(export_all).
-include_lib("eunit/include/eunit.hrl").

read_input(File) ->
    Edges = util:read_input(File, "Step ~c must be finished before step ~c can begin.",
                            fun([[U], [V]]) -> 
                                    {U,V}
                            end),
    make_dag(Edges).

%% to make the tolological sort easy we will create an an adjacency
%% list representation that lists the incoming edges to each node.
make_dag(EdgeList) -> 
    maps:to_list(
      lists:foldl(
        fun({U,V}, AdjacencyList) ->
                maps:update_with(V, fun(IncomingEdges) -> [U|IncomingEdges] end,
                                 [U], AdjacencyList)
        end, maps:from_list([ {X, []} || X <- lists:seq($A, $Z)]), EdgeList)).

time(X) ->
    60 + abs($A - X) + 1.

available_tasks(Dag) ->
    SortedTasks = lists:map(
                    fun(V) ->
                            lists:keyfind(V, 1, Dag)
                    end,
                    dag:topological_sort(Dag)),
    [ A || {A, Deps} <- SortedTasks, length(Deps) == 0 ].

%% Minimize the time needed to complete the instructions represented
%% by the graph.
minimum_time(Dag, NWorkers) ->
    minimum_time(0, [], Dag, NWorkers).

completed(T, Xs) -> lists:filter(fun({X, Started}) -> (T - Started) >= time(X) end, Xs).

refill_active(T, N, Active, Available) ->
    FilteredAvailable = [ A || A <- Available, lists:keysearch(A, 1, Active) == false ],
    Slots = N - length(Active),
    Active ++ lists:map(fun(X) -> {X, T} end, lists:sublist(FilteredAvailable, Slots)).

minimum_time(T, [], [], _) -> T-1;
minimum_time(T, Active, Dag, NWorkers) ->
    C = completed(T, Active),
    {Completed, _} = lists:unzip(C),
    Remaining = lists:subtract(Active, C),
    UpdatedDag = dag:delete(Completed, Dag),
    NewActive = refill_active(T, NWorkers, Remaining, available_tasks(UpdatedDag)),
    minimum_time(T+1, NewActive, UpdatedDag, NWorkers).
    
minimum_time_test_() ->
    [ {"one task one worker",
       ?_assertEqual(time($A), minimum_time([{$A, ""}], 1))},
      {"sequential tasks",
       ?_assertEqual(time($A) + time($B) + time($C),
                    minimum_time([{$A, ""}, {$B, ""}, {$C,""}],1))},
      {"parallel tasks",
       ?_assertEqual(time($A) + time($C),
                     minimum_time([{$A, ""}, {$B, ""}, {$C, ""}], 2))},
      {"parallel dependent tasks",
       ?_assertEqual(time($A) + time($B),
                     minimum_time([{$A, ""}, {$B, "A"}, {$C, ""}], 2))},
      {"parallel three dependent",
       ?_assertEqual(time($A) + time($B),
                     minimum_time([{$A, ""}, {$B, "A"}, {$C, ""}], 3))},
      {"parallel three independent",
       ?_assertEqual(time($C),
                     minimum_time([{$A, ""}, {$B, ""}, {$C, ""}], 3))}
    ].
    
    
    
    
    
