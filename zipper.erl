%% "circular" zipper.
-module(zipper).

-export([from_list/1,
         to_end/1,
         remove/1,
         clockwise/2,
         counterclockwise/2,
         insert/2]).

-include_lib("eunit/include/eunit.hrl").

from_list([X|Right]) ->
    {[], X, Right}.

to_end(Z={_, _, []}) ->
    Z;
to_end(Z) ->
    to_end(clockwise(1, Z)).

clockwise(0, Z) ->
    Z;
clockwise(N, {Left, C, []}) ->
    [NewC|Right] = lists:reverse([C|Left]),
    clockwise(N-1, {[], NewC, Right});
clockwise(N, {Left, C, [R|Right]}) ->
    clockwise(N-1, {[C|Left], R, Right}).

counterclockwise(0, Z) ->
    Z;
counterclockwise(N, {[], C, Right}) ->
    [NewC|Left] = lists:reverse([C|Right]),
    counterclockwise(N-1, {Left, NewC, []});
counterclockwise(N, {[L|Left], C, Right}) ->
    counterclockwise(N-1, {Left, L, [C|Right]}).

insert(X, {Left, C, Right}) ->
    {[C|Left], X, Right}.

remove({[], C, [L|Rest]}) ->
    {C, {[], L, Rest}};
remove({Left, C, []}) ->
    [R|Right] = lists:reverse(Left),
    {C, {[], R, Right}};
remove({Left, C, [R|Right]}) ->
    {C, {Left, R, Right}}.

to_end_test_() ->
    [?_assertEqual({[], 1, []}, to_end(from_list([1]))),
     ?_assertEqual({[3,2,1], 4, []}, to_end(from_list([1,2,3,4])))].

insert_test() ->
    ?_assertEqual({[1], 2, []}, insert(2, from_list([1]))).

remove_test_() ->
    [?_assertEqual({2, {[], 1, []}}, remove({[], 2, [1]})),
     ?_assertEqual({2, {[], 1, []}}, remove({[1], 2, []})),
     ?_assertEqual({3, {[1], 2, []}}, remove({[1], 3, [2]}))].

counterclockwise_test_() ->
    [?_assertEqual({[], 1, []},     counterclockwise(100, from_list([1]))),
     ?_assertEqual({[], 1, [2,3]},  counterclockwise(3, from_list([1,2,3]))),
     ?_assertEqual({[], 1, [2,3]},  counterclockwise(6, from_list([1,2,3]))),
     ?_assertEqual({[2, 1], 3, []}, counterclockwise(7, from_list([1,2,3]))),
     ?_assertEqual({[2, 1], 3, []}, counterclockwise(1, from_list([1,2,3]))),
     ?_assertEqual({[1], 2, [3]},   counterclockwise(2, from_list([1,2,3])))
    ].

clockwise_test_() ->
    [{ "singleton clockwise",  ?_assertEqual({[], 1, []},    clockwise(1, from_list([1]))) },
     { "singleton clockwise*", ?_assertEqual({[], 1, []},    clockwise(10, from_list([1]))) },
     { "three (1)",            ?_assertEqual({[1], 2, [3]},  clockwise(1, from_list([1,2,3]))) },
     { "three (2)",            ?_assertEqual({[2,1], 3, []}, clockwise(2, from_list([1,2,3]))) },
     { "three (3)",            ?_assertEqual({[], 1, [2,3]}, clockwise(3, from_list([1,2,3]))) },
     { "three (6)",            ?_assertEqual({[], 1, [2,3]}, clockwise(6, from_list([1,2,3]))) },
     { "three (7)",            ?_assertEqual({[1], 2, [3]},  clockwise(7, from_list([1,2,3]))) }
    ].

init_test() ->
    ?_assertEqual({[0], 2, [1]}, counterclockwise(1, from_list([1,2,1]))).
