-module(dag).

-export([topological_sort/1, delete/2]).

-include_lib("eunit/include/eunit.hrl").

delete([], Dag)     ->
    Dag;
delete([V|Vs], Dag) ->
    delete(Vs, delete(V, Dag));
delete(V, Dag) ->
    lists:map(fun({K, Edges}) -> {K, lists:delete(V, Edges)} end, lists:keydelete(V, 1, Dag)).

topological_sort(Dag) ->
    topological_sort(Dag, []).

compare({K1, In1}, {K2, In2})
  when length(In1) == length(In2) -> K1 =< K2;
compare({_, In1}, {_, In2})       -> length(In1) < length(In2).

topological_sort([],  Sort) -> lists:reverse(Sort);
topological_sort(Dag, Sort) -> 
    {S, _} =
        lists:foldl(
          fun(X, undefined) -> X;
             (X, Acc)       -> 
                  case compare(X, Acc) of true -> X; false -> Acc end
          end, undefined, Dag),
    topological_sort(delete(S, Dag), [S|Sort]).

topological_sort_test_() ->
    [ {"no dependencies",
       ?_assertEqual("ABC", topological_sort([{$C, []}, {$A, []}, {$B, []}]))},
      {"one dependency",
       ?_assertEqual("BAC", topological_sort([{$A, "B"}, {$C, "B"}, {$B, []}]))},
      {"two dependencies",
       ?_assertEqual("BCA", topological_sort([{$A, "BC"}, {$C, "B"}, {$B, []}]))},
      {"two components",
       ?_assertEqual("ABCD", topological_sort([{$A, []}, {$B, []}, {$C, "A"}, {$D, "B"}]))},
      {"line graph",
       ?_assertEqual("DCBA", topological_sort([{$A, "B"}, {$B, "C"}, {$C, "D"}, {$D, []}]))}
    ].

delete_test_() ->
    [ {"only node", ?_assertEqual([], delete($Z, [{$Z, ""}]))},
      {"one of two", ?_assertEqual([{$A, ""}], delete($B, [{$A, ""}, {$B, ""}]))},
      {"with edges", ?_assertEqual([{$A, "C"}, {$C, ""}],
                                   delete($B, [{$A, "BC"}, {$C, "B"}, {$B, ""}]))},
      {"two out of three",
       ?_assertEqual([{$C, ""}], delete([$A, $B], [{$A, ""}, {$B, "A"}, {$C, "AB"}]))},
      {"delete all", ?_assertEqual([], delete([$A, $B, $C], [{$A, ""}, {$B, "A"}, {$C, "AB"}]))}
    ].
