-module(day9).

-export([high_score/2]).
-include_lib("eunit/include/eunit.hrl").

-define(SPECIAL(X), X rem 23 == 0).

%% 426 players; last marble is worth 72058 points

high_score(Players, Marbles) ->
    simulate(Players, 1, zipper:counterclockwise(1, zipper:from_list([0])),
             array:new([{size, Players}, {fixed, true}, {default, 0}]), Marbles).

simulate(_, Marble, _, Scores, Max) when Marble > Max ->
    lists:max(array:sparse_to_list(Scores));
simulate(Players, Marble, Circle, Scores, Max) when Marble rem 23 == 0 ->
    {X, NewCircle} = zipper:remove(zipper:counterclockwise(7, Circle)),
    Player = Marble rem Players,
    NewScores = array:set(Player, Marble + X + array:get(Player, Scores), Scores),
    simulate(Players, Marble+1, NewCircle, NewScores, Max);
simulate(Players, Marble, Circle, Scores, Max) ->
    simulate(Players, Marble+1, zipper:insert(Marble, zipper:clockwise(1,Circle)), Scores, Max).

high_score_test_() ->
    [?_assertEqual(32,     high_score(9, 25)),
     ?_assertEqual(8317,   high_score(10, 1618)),
     ?_assertEqual(146373, high_score(13, 7999)),
     ?_assertEqual(2764,   high_score(17, 1104)),
     ?_assertEqual(54718,  high_score(21, 6111)),
     ?_assertEqual(37305,  high_score(30, 5807))].
